def dictionary_field_ops(): #We have to be sure about these steps

    dictionary={
        "stages":[
            '01.  Upcoming',
            '02.  Awaiting Recognition',
            '03.  Awaiting Assessment',
            '04.  Awaiting Project',
            '05.  Awaiting Production',
            '06.  Awaiting Shipping',
            '07. ️ Awaiting Installation',
            '08. ✅ Done',
            '09. ❌ Cancelled',
            '10.  Investigation'
        ]
    }
    return dictionary

def dictionary_smx_ops(): #We have to be sure about the stages itselves

    dictionary={
        "stages":[
            ' Investigation',
            ' No Technical Solution',
            '01.  Awaiting Assessment',
            '02.  Awaiting Project',
            '03.  Ready for Order',
            '04.  Awaiting Production',
            '05. ✋ Awaiting Installation',
            '06. ️ Installation Scheduled',
            '07. ▶️ Installation In Progress',
            '08.  Awaiting Rework',
            '09. Installation on Hold', #Não tem na base de dados de query, logo não faz diferença na nossa abordagem
            '10. Awaiting Network', #Não tem na base de dados de query, logo não faz diferença na nossa abordagem
            '11. ⏸️ Awaiting Validation',
            '12. ✅ Installation Complete'
        ]
    }
    return dictionary

#Now, it is time to test the integration of data with our code