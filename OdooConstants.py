class OdooConstants: #We have to put in this class the constants we are going to use in
    #Odoo, so th fields that have to be filled

    def __init__(self, model,env): #Env is the environment of Odoo we are talking about
        self.env= env
        self.model= model #The model if it is project.project or project.tasks

    #def getSearchFilter(self): #I do not know the purpose of this code
        #return {
           # "fields":{
             #   "name": "name",
            #    "email": "work_email"
           # ,
          #      "logicOperations": {
         #           "ilike": "ilike"
        #        }
       # }

    def FieldOpsFields(self):

        if self.env=="TST":
            if self.model=="project.project": #This is the module of projects in Odoo
                return { 
                    "name":"name",
                    "customer":"partner_id",
                    "created_by":"create_uid",
                    "orderLine":{
                        "Smx_Ops":"task_id",
                        "Smx_Ops_Stage":"x_studio",
                        "Field_Ops":"project_id",
                        "Employee":"employee_id"
                    },
                    "Name_of_the_Tasks": "label_tasks",
                    "name_of_the_project":"project_id",
                    "stage":"stage_id",
                    "project":"project_id"
            }
            else:
                if self.model=="project.task": #This is the model of tasks in Odoo
                    return {
                        "name":"name", #The stage of SMX Ops
                        "project":"project_id", #we have here to put the corresponding ID associated
                        "orderLine":{
                            "smx_ops_stage":"x_studio_stage_1",
                            "employee":"employee_id"
                        },
                        "stage":"stage_id"
                    }
#As our integration is going forward, we are going to put more data here in the dictionaries