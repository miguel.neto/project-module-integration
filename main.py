from tokens import NOTION_TOKEN,DATABASE_ID,DATABASE_ID2
import requests
import json
import OdooAPI
from NotionStages import dictionary_field_ops, dictionary_smx_ops

def query_information(database_id):
    url=f"https://api.notion.com/v1/databases/{database_id}/query"

    headers={
        "Authorization":  "Bearer " + NOTION_TOKEN,
        "Content-type": "application/json",
        "Notion-Version": "2022-06-28",
    }

    payload={"page_size": 100}

    response= requests.post(url, headers= headers, json=payload)

    pages_and_databases=[]
    
    if response.ok:
        new_dict= response.json() #Converts the json format to a dictionary
        pages_and_databases= new_dict.get("results",[])        

        while new_dict.get("has_more"): #We just do a "get" when we obtain a list

            payload["start_cursor"]= new_dict.get("next_cursor") #Just works in dictionaries

            response= requests.post(url, json= payload, headers= headers)
            if response.ok:
                new_dict= response.json() #Of course, we will have one page because we are updating the dictionary itself
                pages_and_databases.extend(new_dict.get("results", [])) #Here we extend our list with a new list

    return pages_and_databases

def write_to_json(data, file_name):

    with open(file_name,"w") as f:
        json.dump(data,f)


def retrieve_properties_of_a_page(page_id: str):
    url=f"https://api.notion.com/v1/pages/{page_id}"

    headers={
        "Authorization": "Bearer " + NOTION_TOKEN,
        "Content-Type": "application/json",
        "Notion-Version":"2022-06-28"
    }

    response= requests.get(url, headers= headers)

    return response.json()

data_in_list_field_ops=query_information(DATABASE_ID)
data_in_list_smx_ops=query_information(DATABASE_ID2)

data1= OdooAPI.OdooAPI("TST", "https://smartex.testes.exo.pt","hORcLieVXOr4MbLxgcPRemZGQ9dNyq7cHtRXZ7JC","d9ARI9O9wKhRc0cypl1PPEuj7XyyrnH5EnzLPaaj","project.project")
authenticate= data1.authenticate()

# We have to create a function that converts the list into a dictionary

def convert_list_to_dict_of_dicts(data: list, include_key_field: bool = True):
    #We have the key with the value and the corresponding key itself

    new_dict = {}

    for i, dictionary in enumerate(data, start=1):
        inner_dict = {k: v for k, v in dictionary.items()}
        if not include_key_field and 'name' in inner_dict:
            inner_dict.pop('name', None)
        new_dict[i] = inner_dict

    return new_dict

def discover_stage_id_field_ops(fieldops: str, fieldops_list: list, fieldops_stages_list: list):
    #The string associated with Field Ops
    #The list with the strings of Field Ops
    #The list with the stages of Field Ops

    a= ""
    for i in range(len(fieldops_list)):
        if fieldops == fieldops_list[i]:
            a=fieldops_stages_list[i] #This the stage we want to find for the Field Ops
            break
    # Now we have to find the ID associated with the Field Ops stage and return that value


    new_dict= data1.id_stage_field_ops()
    id=0

    for keys, values in new_dict.items():
        if keys== "ids":
            if a==dictionary_field_ops()["stages"][0]:
                id= data1.id_stage_field_ops()["ids"]["Upcoming"]
            else:
                if a==dictionary_field_ops()["stages"][1]:
                    id= data1.id_stage_field_ops()["ids"]["Awaiting Recognition"]
                else:
                    if a==dictionary_field_ops()["stages"][2]:
                        id=data1.id_stage_field_ops()["ids"]["Awaiting Assessment"]
                    else:
                        if a== dictionary_field_ops()["stages"][3]:
                            id=data1.id_stage_field_ops()["ids"]["Awaiting Project"]
                        else:
                            if a== dictionary_field_ops()["stages"][4]:
                                id= data1.id_stage_field_ops()["ids"]["Awaiting Production"]
                            else:
                                if a== dictionary_field_ops()["stages"][5]:
                                    id= data1.id_stage_field_ops()["ids"]["Awaiting Shipping"]
                                else:
                                    if a==dictionary_field_ops()["stages"][6]:
                                        id= data1.id_stage_field_ops()["ids"]["Awaiting Installation"]
                                    else:
                                        if a== dictionary_field_ops()["stages"][7]:
                                            id= data1.id_stage_field_ops()["ids"]["Done"]
                                        else:
                                            if a==dictionary_field_ops()["stages"][8]:
                                                id= data1.id_stage_field_ops()["ids"]["Canceled"]
                                            else:
                                                if a== dictionary_field_ops()["stages"][9]:
                                                    id= data1.id_stage_field_ops()["ids"]["Investigation"]
    return id


def discover_stage_id_smxops(smxops: list, smxops_list: list, smxops_stages_list: list):

    a=[]
    for j in range(len(smxops)):
        for i in range(len(smxops_list)):
            if smxops[j] == smxops_list[i]:
                a[j]= smxops_stages_list[i]
    #Now, that we have built a, we need to know how to compare the terms
    
    new_dict= data1.id_stage_smx_ops()
    id=[]
    for i in range(len(a)):
        for keys, values in new_dict.items():
            if keys=="ids":
                if a[i]== dictionary_smx_ops()["stages"][0]:
                    id.append(data1.id_stage_smx_ops()["ids"]["Investigation"])
                else:
                    if a[i]== dictionary_smx_ops()["stages"][1]:
                        id.append(data1.id_stage_smx_ops()["ids"]["No Technical Solution"])
                    else:
                        if a[i]== dictionary_smx_ops()["stages"][2]:
                            id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Assessment"])
                        else:
                            if a[i]== dictionary_smx_ops()["stages"][3]:
                                id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Project"])
                            else:
                                if a[i]== dictionary_smx_ops()["stages"][4]:
                                    id.append(data1.id_stage_smx_ops()["ids"]["Ready for Order"])
                                else:
                                    if a[i]== dictionary_smx_ops()["stages"][5]:
                                        id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Production"])
                                    else:
                                        if a[i]== dictionary_smx_ops()["stages"][6]:
                                            id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Installation"])
                                        else:
                                            if a[i]== dictionary_smx_ops()["stages"][7]:
                                                id.append(data1.id_stage_smx_ops()["ids"]["Installation Scheduled"])
                                            else:
                                                if a[i]== dictionary_smx_ops()["stages"][8]:
                                                    id.append(data1.id_stage_smx_ops()["ids"]["Installation in Progress"])
                                                else:
                                                    if a[i]== dictionary_smx_ops()["stages"][9]:
                                                        id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Rework"])
                                                    else:
                                                        if a[i]== dictionary_smx_ops()["stages"][10]:
                                                            id.append(data1.id_stage_smx_ops()["ids"]["Installation on Hold"])
                                                        else:
                                                            if a[i]== dictionary_smx_ops()["stages"][11]:
                                                                id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Network"])
                                                            else:
                                                                if a[i]== dictionary_smx_ops()["stages"][12]:
                                                                    id.append(data1.id_stage_smx_ops()["ids"]["Awaiting Validation"])
                                                                else:
                                                                    if a[i]== dictionary_smx_ops()["stages"][13]:
                                                                        id.append(data1.id_stage_smx_ops()["ids"]["Installation Complete"])
    
    return id

def convert_dict_to_list(data: dict): #Now, we just a list with all the values of SMX Ops

    lista=[]


    for keys, values in data.items():
        lista.append(values)
    
    return lista #This is going to be a list of lists

def obtain_list_elements(data: list): #This function will just work of GetSMXOps_stages

    lista=[]

    for i in range(len(data)):
        for keys, values in data[i].items():
            lista.append(values)

    return lista

def run_the_program(fieldops_list: list, smxops_list: list, dictionary: dict):
    '''
    The goal of this function is to run all the program and then to post the information to Odoo from Notion
    '''
    import GetData

    new_data= GetData.getSMXOPS(smxops_list) #Dict with Field Ops and SMX Ops

    new_data1= GetData.getFieldOps(fieldops_list) #Names of Field Ops
    new_dataa1= GetData.getSMXOPS(smxops_list) #The names of SMX Ops

    new_dataa1= convert_dict_to_list(new_dataa1)

    new_data2= GetData.getFieldOpsStage(fieldops_list) #Stages of Field Ops
    new_dataa2= GetData.getSMXOPS_stages(smxops_list) #Stages of SMX Ops

    new_dataa2= obtain_list_elements(new_dataa2)
    dbFilter= {"inOdoo": False}
    index=0

    for keys, values in new_data.items():
        index+=1

        fieldopsdata,nothing_smx_ops_data= data1.final_payload({keys:values},0,dictionary["Project"],[],discover_stage_id_field_ops(str(keys),new_data1, new_data2))
        new_fieldopsdata= convert_list_to_dict_of_dicts(fieldopsdata)

        for keys, dictionaries in new_fieldopsdata.items():
            data1.createFieldOps(dictionary["Project"], **dictionaries)

        projectID= sorted(data1.searchByField("project.project"))[-1]

        stages_id,new_dictt= data1.stagess_id() 

        for i in range(0,14):

            #Will associate the SMX Ops with the project ID

            task_stage ={
                'name': new_dictt["stages"][i],
                'project_ids': [projectID], 
                'sequence': i
            }
            data1.create_stages_of_SMXOPS("project.task.type",**task_stage)

            #Próximo passo é associar a fase associada ao projeto
            #Quando importamos o Field Ops, temos que ter um estágio específico associado
        #The break point is here, so I have to fix what I have to fix right now
        payload2linha, smxopsdata= data1.final_payload({keys:values}, projectID,dictionary["Task"], discover_stage_id_smxops(str(values),new_dataa1,new_dataa2),0)

        new_smxopsdata=convert_list_to_dict_of_dicts(smxopsdata)

        for keys,dictionaries in new_smxopsdata.items():
            data1.createFieldOps(dictionary["Task"], **dictionaries)

    return index

dictionary={"Project":"project.project","Task":"project.task"}

print(run_the_program(data_in_list_field_ops, data_in_list_smx_ops, dictionary))