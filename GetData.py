from RemoveEmojis import remove_emojis
'''
    This code will provide all the data retrieved from Notion with all the properties needed to, next,
           export the data to Odoo (By now, we just enough data to export the stages and ID's associated,
           not the properties; however, this should be done easily, we are going to create and export the data for
           each of the Field Ops and SMX Ops, one code for Field Ops and one code for SMX Ops)
'''

def getFieldOpsName_from_SMXOPS(data:list):
    #With the SMX Ops DB, is possible to retrieve information from the Field Ops names

    field_ops_name=[]

    for i in range(len(data)):

        list= data[i]["properties"]["🤖🛠️ Field Ops"]["formula"]

        for keys, values in list.items():
            if keys=="string":
                field_ops_name.append(list[keys])

    return field_ops_name


def getFieldOpsStage(data:list): #Gets the stages of Field Ops Stage by order in Field Ops DB
    field_ops_stage=[]

    for i in range(len(data)):
        list=data[i]["properties"]["✍️🛠️ Status"]["select"]
        
        if list: #If the is not a None Type Object

            for keys,values in list.items():
                if keys=="name":
                    list[keys]= remove_emojis(list[keys])
                    field_ops_stage.append(list[keys])

    return field_ops_stage #This is a list as well, with all the stages of Field Ops

def getFieldOps(data: list): #Gives the names of the Field Ops in the Field Ops DB
    # data is the field ops DB
    field_ops_name=[]

    for i in range(len(data)):

        list= data[i]["properties"]["✍️🛠️ Name"]["title"]

        for keys, values in list[0].items():
            if keys=="text":
                field_ops_name.append(list[0]["text"]["content"])

    return field_ops_name

def getSMXOPS(data:list):

    smx_ops_name=[]

    new_dict={}

    fieldops= getFieldOpsName_from_SMXOPS(data) #List with the Field Ops names

    for i in range(len(data)):

        list= data[i]["properties"]["✍️⚙️ Name"]["title"]

        for keys, values in list[0].items():
            if keys=="text":
                smx_ops_name.append(list[0]["text"]["content"])

    for i in range(len(smx_ops_name)): #O comprimento do Field Ops name é o mesmo que o do SMX Ops name
        if fieldops[i]!='':
            if fieldops[i] not in new_dict.keys():
                new_dict[fieldops[i]]= [smx_ops_name[i]]
            else:
                if fieldops[i] in new_dict.keys():
                    new_dict[fieldops[i]].append(smx_ops_name[i])
        else:
            if fieldops[i]=='':
                print("Please, go to Notion and update the Field Ops inside the SMX Ops")

    return new_dict #We do not want to add the SMX Ops without the Field Ops itself

def getSMXOPS_stages(data:list): #Changes the elements of the values to the phases of the SMX Ops

    smx_ops_name=[]

    new_dict_stages={}

    fieldops= getFieldOpsName_from_SMXOPS(data)

    for i in range(len(data)):

        list= data[i]["properties"]["✍️⚙️ Install. Status"]["status"] #Stage of the list

        for keys, values in list.items():
            if keys=="name":
                data_to_add= remove_emojis(list["name"])
                smx_ops_name.append(data_to_add)

    #Now we are going to construct a dictionary with the names of the Field Ops and the name of SMX Ops

    for i in range(len(smx_ops_name)):
        if fieldops[i] !='':
            if fieldops[i] not in new_dict_stages.keys():
                new_dict_stages[fieldops[i]]= [smx_ops_name[i]]
            else:
                if fieldops[i] in new_dict_stages.keys():
                    new_dict_stages[fieldops[i]].append(smx_ops_name[i])
        else:
            if fieldops[i]=='':
                print("Go to Notion and update the Field Ops name")

    return [new_dict_stages] #Dict with Field Ops and corresponding SMX Ops

def stages_with_stages(data: list, data1: dict):

    new_dict={}

    for i in range(len(data)):
        for j in data1.keys():
            if getFieldOpsStage(data)[i] in new_dict.keys():
                pass
            else:
                if getFieldOpsStage(data)[i] not in new_dict.keys():
                    if data[i]["properties"]["✍️🛠️ Name"]["title"][0]["text"]["content"]== j:
                        field_ops= getFieldOpsStage(data)[i]
                        new_dict[field_ops]= data1[j]

    return new_dict