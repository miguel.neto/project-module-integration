# This program will interact with the Odoo's api in order to post there information
import json
import requests
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
import OdooConstants

#These functions are absolutely general and have to be used in our context

#Write now the code that will interact with Odoo's API

class OdooAPI:

    def __init__(self, env, base_url, client_id, client_secret, model):

        self.base_url=base_url
        self.client_id=client_id
        self.client_secret= client_secret
        self.client= BackendApplicationClient(client_id= client_id)
        self.oauth= OAuth2Session(client= self.client)
        self.token= None
        self.constants= OdooConstants.OdooConstants(model,env)
        self.env= env
        self.model= model

    def authenticate(self):
        self.token= self.oauth.fetch_token(
            token_url= f"{self.base_url}/api/v2/authentication/oauth2/token",
            client_id= self.client_id,
            client_secret= self.client_secret
        )

        return self.token
#What is the logicOperator we should do in order to fulfill our code

    def searchByField(self, model=None, fieldName=None, logicOperator=None, value=None): #This will find the field we
        # want, in order to, after, input information associated
        if fieldName!= None and logicOperator!= None and value!= None:
            params = { #We have filters, but we can have 0 paramerts, and all the data is retrieved
                'domain': '["List [ \\"' + fieldName + '\\", \\"' + logicOperator + '\\", \\"' + value + '\\" ]"]'
            }
        else:
            params={}

        url= f"{self.base_url}/api/v2/search/{model}" #The url associated with the model
        response= self.oauth.get(url, params= params) #Searches for the IDs of a given model, with a specific param

        if response.status_code==200:
            responses_id= response.json()

            return responses_id
        #else:
            #print("Failed to search: ", response.status_code)

    def read_model(self,model, id):
        #if not self.token:
            #print("Not authenticated. Authenticate first")
            #return

        url=f"{self.base_url}/api/v2/read/{model}?ids={id}"
        response= self.oauth.get(url)

        if response.status_code== 200:
            data_read= response.json()
            with open("data", "w") as f:
                json.dump(data_read, f)

        return data_read
    
    def stagess_id(self):
        dictionary={
            "stages":[
                "Investigation",
                "No Technical Solution",
                "Awaiting Assessment",
                "Awaiting Project",
                "Ready for Order",
                "Awaiting Production",
                "Awaiting Installation",
                "Installation Scheduled",
                "Installation in Progress",
                "Awaiting Rework",
                "Installation on Hold",
                "Awaiting Network",
                "Awaiting Validation",
                "Installation Complete"
            ]
        }
        array_ids=[]

        for keys, values in dictionary.items():
            if keys=="stages":
                for i in range(len(values)):
                    ID= self.searchByField("project.task.type","name","=",values[i])
                    ID=ID[-1]
                    array_ids.append(ID)
        
        return array_ids, dictionary

    def createFieldOps(self,model, **kwargs):

        #if not self.token:
            #print("Not authenticated. Please authenticate first")
            #return
        
        createUrl = f"{self.base_url}/api/v2/create/{model}"

        payload_final = str(kwargs)
        params = {
            "values": payload_final
        }
        
        response = self.oauth.post(createUrl, params = params) #As we want to add information to odoo, we have to use the "post" request
        data = response.json()
        #if response.status_code!=200:
            #print(f"The status is {response.status_code}")

        #else:
            #print(f"The status code is {response.status_code}")
    

    def create_stages_of_SMXOPS(self, model, **kwargs): #In this case, the model will be specifically the model
        # of project.task.type

        #if not self.token:
           # print("Not Authenticated.Please, authenticate first")
        
        createURL=f"{self.base_url}/api/v2/create/{model}"

        payload_final=str(kwargs)

        params= {
            "values": payload_final
        }
        response = self.oauth.post(createURL, params = params) #As we want to add information to odoo, we have to use the "post" request
        data = response.json()
        #if response.status_code!=200:
            #print(f"The status is {response.status_code}")

        #else:
            #print(f"The status code is {response.status_code}")

    def final_payload(self, data, project_id, model,stage_id_list: list, stage_id1: int): #The data is a dictionary with Field Ops and the corresponding SMX Ops (list format)
        # but just one Field Ops to facilitate our work and the corresponding SMX Ops to facilitate our work as well
        #In this case, the SMX Ops associated will be a list associated with the Field Ops, just a key-value

        FieldOps_list=[]

        SMXOps_list=[]

        #The index of the Field Ops we are going to input in there

        #The keys are the Field Ops itself
        self.constants1= OdooConstants.OdooConstants("project.task","TST")
        #print(self.constants1.FieldOpsFields()["name"])

        #The values are the SMX Ops itself

        if model== "project.project":
            for keys, values in data.items():
                new_dict={self.constants.FieldOpsFields()["name"]: keys, self.constants.FieldOpsFields()["stage"]:stage_id1}
                FieldOps_list.append(new_dict) #We have to add a dict inside the Field Ops_list because this is
            # a list of dictionaries
        else:
            if model== "project.task":
                for keys, values in data.items(): #Now, we have to create the Field Ops with the data we have provided in order to associate the corresponding SMX Ops
                    if len(values)>1:
                        for i in range(len(values)): #Run over the SMX OPS associated with a specific Field Ops
                            new_dict1={self.constants1.FieldOpsFields()["name"]: values[i], self.constants1.FieldOpsFields()["project"]:project_id, self.constants1.FieldOpsFields()["stage"]: stage_id_list[i]} 
                            SMXOps_list.append(new_dict1)
                    else:
                        if len(values)==1:
                            new_dict1={self.constants1.FieldOpsFields()["name"]:values, self.constants1.FieldOpsFields()["project"]: project_id, self.constants1.FieldOpsFields()["stage"]:stage_id_list[0]}
        return FieldOps_list, SMXOps_list
    
    #This will be a list of dictionaries or just a dictionary inside the list itself
    
    def id_stage_field_ops(self): #These are stages associated with the Field Ops with the corresponding id's of
        #Field Ops
        
        dictionary={
            "ids":{
                "Upcoming": 50,
                "Awaiting Recognition": 41,
                "Awaiting Assessment": 42,
                "Awaiting Project": 21,
                "Awaiting Production": 43,
                "Awaiting Shipping": 44 ,
                "Awaiting Installation": 22,
                "Done": 31,
                "Canceled": 48,
                "Investigation": 49 
                }
                }
        return dictionary
    
    def id_stage_smx_ops(self):

        dictionary={}

        dictionary={
            "ids":{
            "Investigation": 258,
            "No Technical Solution": 260,
            "Awaiting Assessment": 262,
            "Awaiting Project": 263,
            "Ready for Order": 304,
            "Awaiting Production": 264,
            "Awaiting Installation": 265,
            "Installation Scheduled": 266,
            "Installation in Progress": 267,
            "Awaiting Rework": 268,
            "Installation on Hold": 305,
            "Awaiting Network": 270,
            "Awaiting Validation": 271,
            "Installation Complete": 272
        }
        }
        return dictionary

#data1= OdooAPI("TST","https://smartex.testes.exo.pt","hORcLieVXOr4MbLxgcPRemZGQ9dNyq7cHtRXZ7JC","d9ARI9O9wKhRc0cypl1PPEuj7XyyrnH5EnzLPaaj","project.project")
#data1.authenticate()